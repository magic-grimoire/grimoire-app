import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | decks.deck', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:decks.deck');
    assert.ok(route);
  });
});
