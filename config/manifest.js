/* eslint-env node */
'use strict';

module.exports = function(/* environment, appConfig */) {
  // See https://github.com/san650/ember-web-app#documentation for a list of
  // supported properties

  return {
    name: "Grimoire",
    short_name: "Grimoire",
    description: "Card game application",
    start_url: "/",
    display: "standalone",
    background_color: "#fff",
    theme_color: "#fff",
    icons: [{
      src: "/public/logo/grimoire.jpeg",
      sizes: "512x512",
      type: "image/jpeg"
    }],
    ms: {
      tileColor: '#fff'
    }
  };
}
