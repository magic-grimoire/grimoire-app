import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function () {
  this.route('games', function () {
    this.route('new');
    this.route('game', { path: '/:game_id' }, function () {
      this.route('start');
    })
  });

  this.route('decks', function () {
    this.route('new');
    this.route('deck', { path: "/:deck_id" }, function() {});
  });
});

export default Router;
