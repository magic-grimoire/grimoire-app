import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({
    model() {
        return RSVP.hash({
            games: this.get('store').findAll('game'),
            decks: this.get('store').findAll('deck')
        });
    }
});
