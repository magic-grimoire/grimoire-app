import Route from '@ember/routing/route';

export default Route.extend({
    model() {
        let { game_id } = this.paramsFor('games.game')
        return this.get('store').findRecord('game', game_id, { include: 'decks'     })
    }
});
