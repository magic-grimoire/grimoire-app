import Route from '@ember/routing/route';

export default Route.extend({
    model() {
        let { deck_id } = this.paramsFor('decks.deck')
        return this.get('store').findRecord('deck', deck_id)
    }
});
