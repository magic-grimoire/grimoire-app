import DS from 'ember-data';

export default DS.Model.extend({
    deckA: DS.belongsTo('deck'),
    scoreA: DS.attr('number'),
    deckB: DS.belongsTo('deck'),
    scoreB: DS.attr('number'),
    date: DS.attr('date')
});
