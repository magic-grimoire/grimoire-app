import Controller from '@ember/controller';
import { computed } from '@ember/object'

export default Controller.extend({
    winner: computed('model.scoreA', 'model.scoreB', function () {
        return this.get('model.scoreA') > this.get('model.scoreB')
            ? this.get('model.deckA')
            : this.get('model.deckB')
    })
});
