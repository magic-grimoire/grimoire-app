import Controller from '@ember/controller';
import { computed } from '@ember/object'

export default Controller.extend({

    counterA: {
        hp: 20,
        poison: 0,
        wins: 0
    },

    counterB: {
        hp: 20,
        poison: 0,
        wins: 0
    },

    gameOver: computed('counterA.hp', 'counterA.poison', 'counterB.hp', 'counterB.poison', function () {
        return this.counterA.hp === 0
            || this.counterA.poison === 10
            || this.counterB.hp === 0
            || this.counterB.poison === 10
    }),

    winner: computed('gameOver', function () {
        if (this.get('gameOver')) {
            if (this.counterA.hp === 0 || this.counterA.poison === 10) {
                this.update('counterB', 'wins', 1)
                return this.get('model.deckB')
            } else {
                this.update('counterA', 'wins', 1)
                return this.get('model.deckA')
            }
        } else {
            return undefined
        }
    }),

    update(counter, type, amount) {
        let newValue = this[counter][type] + amount
        newValue = newValue < 0 ? 0 : newValue
        newValue = (type === 'poison' && newValue > 10) ? 10 : newValue
        this.set(`${counter}.${type}`, newValue)
    },

    actions: {
        reset() {
            this.set('counterA.hp', 20)
            this.set('counterA.poison', 0)
            this.set('counterB.hp', 20)
            this.set('counterB.poison', 0)
        },
        update(counter, type, amount) {
            if (!this.get('gameOver')) {
                this.update(counter, type, amount)
            }
        },
        save() {
            this.set('model.scoreA', this.get('counterA.wins'))
            this.set('model.scoreB', this.get('counterB.wins'))
            this.set('model.date', new Date())
            this.get('model')
                .save()
                .then((game) => this.transitionToRoute('games.game', { game_id: game.id }))
        },
        discard() {
            this.get('model')
                .destroyRecord()
                .then(() => this.transitionToRoute("games"))
        }
    }
});
