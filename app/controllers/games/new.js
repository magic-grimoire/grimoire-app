import Controller from '@ember/controller';
import { computed } from '@ember/object';

export default Controller.extend({

    deckA: undefined,
    deckB: undefined,

    isValid: computed('deckA', 'deckB', function () {
        return this.get('deckA') != undefined && this.get('deckB') != undefined
    }),

    decks: computed(function () {
        return this.get('store').findAll('deck')
    }),

    actions: {
        startGame() {
            this.get('store')
                .createRecord('game', {
                    deckA: this.get('store').peekRecord('deck', this.get('deckA')),
                    deckB: this.get('store').peekRecord('deck', this.get('deckB'))
                })
                .save()
                .then((newGame) => {
                    this.transitionToRoute('games.game.start', { game_id: newGame.id })
                }, () => {
                    // Handle errors
                });
        }
    }
});
