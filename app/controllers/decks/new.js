import Controller from '@ember/controller';
import { computed } from '@ember/object';

export default Controller.extend({
    name: '',

    isValid: computed('name', function() {
        return this.get('name').length > 3;
    }),

    actions: {
        save() {
            this.get('store')
                .createRecord('deck', {
                    name: this.get('name'),
                    elo: 100
                })
                .save()
                .then((deck) => this.transitionToRoute('decks.deck', {deck_id: deck.id}))
        }
    }

});
