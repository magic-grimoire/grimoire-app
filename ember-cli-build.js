'use strict';

const EmberApp = require('ember-cli/lib/broccoli/ember-app');
const pickFiles = require('broccoli-static-compiler');

module.exports = function (defaults) {
  let app = new EmberApp(defaults, {
    'asset-cache': {
      include: [
        'assets/**/*'
      ]
    },
    'esw-cache-fallback': {
      patterns: ['/'],
      version: '1'
    }
  });

  return app.toTree();
};
